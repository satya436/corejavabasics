package satyasai;

public class A3 {

	
	 public static void main(String[] args) {
			
		 System.out.println("this is main");
		 A3 obj=new A3();
		 A3 obj1=new A3(3,4);
		 
	}
	
	 static{  
		  System.out.println("static block is invoked");  
		 // System.exit(0);  
		  
		  }  
	
	 {
		 System.out.println("this is instant block");
	 }
	
	 
	 A3(){ 
		 System.out.println("this is default constructor");
	 }
	 
	 A3(int a,int b){
		 System.out.println("this parameter constructor"+(a+b));
	 }
	 
}
