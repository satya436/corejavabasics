package satyasai;

public class ConcatTestSBvsSB {

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		StringBuffer sb = new StringBuffer("sai");
		for (int i = 0; i < 10000; i++) {
			sb.append("satya");
		}
		System.out.println("Time taken by StringBuffer: " + (System.currentTimeMillis() - startTime) + "ms");
		startTime = System.currentTimeMillis();
		StringBuilder sb2 = new StringBuilder("sai");
		for (int i = 0; i < 10000; i++) {
			sb2.append("satya");
		}
		System.out.println("Time taken by StringBuilder: " + (System.currentTimeMillis() - startTime) + "ms");
	}
}

//String Java & Tpoint
//Time taken by StringBuffer: 2ms
//Time taken by StringBuilder: 8ms

//String satya & sai
//Time taken by StringBuffer: 3ms
//Time taken by StringBuilder: 2ms

//String sai & satya

//Time taken by StringBuffer: 1ms
//Time taken by StringBuilder: 7ms


