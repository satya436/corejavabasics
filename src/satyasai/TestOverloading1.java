package satyasai;

class Adder {
	static double add(double a, double b) {
		return a + b;
	}

	static double add(double a, double b, int c) {
		return a + b + c;
	}
}

class TestOverloading1 {
	public static void main(String[] args) {
		
		System.out.println("Two parameters method:"+" "+Adder.add(11.4, 11.4));
		
		System.out.println("Three Parameter Methods:"+" "+Adder.add(11.4, 11.4, 11));
	}
	
	
}