package satyasai;

class Adder1 {
	static int add(int a, int b) {
		return a + b;
	}

	static double add(double a, double b) {
		return a + b;
	}
}

class TestOverloading2 {
	public static void main(String[] args) {
		System.out.println("The adder using Different data types with same PARAMETERS:"+" "+Adder1.add(11.4, 11));
		System.out.println("The adder using Different data types with same PARAMETERS:"+" "+Adder1.add(12.4, 12.6));
	}
}