package satyasai;

public class InstanceTest {
	public static void main(String args[]) {
		System.out.println("Hashcode test of String:");
		String str = "00000000456";
		System.out.println(str.hashCode());
		str = str + "00000000455";
		System.out.println(str.hashCode());

		System.out.println("Hashcode test of StringBuffer:");
		StringBuffer sb = new StringBuffer("00000000456");
		System.out.println(sb.hashCode());
		sb.append("00000000455");
		System.out.println(sb.hashCode());
	}
}

//Strings Java & Tpoint
/*Hashcode test of String:
3254818
229541438
Hashcode test of StringBuffer:
366712642
366712642
*/

//String sai & satya

/*Hashcode test of String:
113627
1833954899
Hashcode test of StringBuffer:
366712642
366712642

 */

//String 00000000456 & 00000000455

/*Hashcode test of String:
1065529813
-723126049
Hashcode test of StringBuffer:
366712642
366712642
*/
