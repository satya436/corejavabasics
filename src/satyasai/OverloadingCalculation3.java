package satyasai;

class OverloadingCalculation3 {
	void sum(double a, long b) {
		System.out.println("a method invoked");
	}

	void sum(double a, double b) {
		System.out.println("b method invoked");
	}

	public static void main(String args[]) {
		OverloadingCalculation3 obj = new OverloadingCalculation3();
		obj.sum(20.4, 20);// now ambiguity
	}
}