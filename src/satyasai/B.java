package satyasai;

class B {
	public static void main(String args[]) {
		StringBuilder sb = new StringBuilder("Hello ");
		
		sb.append("Java");// now original string is changed
		System.out.println(sb);// prints Hello Java
		
		sb.insert(1,"Java");//now original string is changed  
		System.out.println(sb);//prints HJavaello Java 
		
		sb.replace(1,3,"sai");  
		System.out.println(sb);//prints Hsaivaello Java
		
		sb.delete(1,3);  
		System.out.println(sb);//prints Hivaello Java
		
		sb.reverse();  
		System.out.println(sb);//prints avaJ olleaviH
		
		System.out.println(sb.capacity());
		
	}
	
}